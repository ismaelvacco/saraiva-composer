UPDATE core_config_data SET value="http://saraiva-magento.localhost/" WHERE path in ("web/unsecure/base_url", "web/secure/base_url");
UPDATE core_config_data SET value='saraiva-magento.localhost' WHERE path like 'web/cookie/cookie_domain';
UPDATE admin_user SET password = CONCAT(SHA2('admin', 256), ':xxxxxxxx:1') WHERE username = 'admin';
