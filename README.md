## Saraiva Composer
### Development environment

#### Start environment
```
$ docker-compose up -d app phpfpm db redis mongodb
```

#### setup
```
$ docker-compose up -d setup
```

after install mysql dump
```
$ docker-compose exec phpfpm bash -c 'cd /var/www/html; php shell/Mongo/mongo.php 0 99999999'
```

running OMS integration
```
$ docker-compose exec phpfpm bash -c 'cd /var/www/html; php shell/SalesIntegrationNew/index.php OMS'
```
